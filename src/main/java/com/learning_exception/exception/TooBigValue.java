package com.learning_exception.exception;

public class TooBigValue extends RuntimeException {
    String name;

   public  TooBigValue(String name) {
        this.name = name;
    }
    public void what(){
        System.out.println("The value is too big for "+name);
    }
}
