package com.learning_exception.basicClasses;

import com.learning_exception.exception.BelowOrEqualZero;
import com.learning_exception.exception.TooBigValue;

import java.util.Scanner;

public class LoveStory extends Book {

    int year;

    LoveStory() throws TooBigValue, BelowOrEqualZero{
        super.setNameOfBook();
        super.setNameOfAuthor();
        genre  = "love story";
        checkYear();
    }

    @Override
    public String getGenre() {
        return "book about love";
    }

    private void checkYear() throws TooBigValue, BelowOrEqualZero {


        Scanner sc = new Scanner(System.in);
        int addedYear = 0;
        boolean wrongData;
        do{
            wrongData = false;
        System.out.print("Enter the year");
        addedYear = sc.nextInt();
        try {
            if (addedYear >= 2020) {
                throw new TooBigValue("year");
            } else if (addedYear <= 0) {
                throw new BelowOrEqualZero();
            }
        } catch (TooBigValue ex) {
            wrongData = true;
            ex.what();
            System.out.println("Try again!");
        } catch (BelowOrEqualZero ex) {
            wrongData = true;
            ex.what();
            System.out.println("Try again!");
        }

    }while(wrongData);
        year = addedYear;
    }

    @Override
    public String toString() {
        return super.toString() + "\t" + year + "year";
    }
}
