package com.learning_exception.basicClasses;
import com.learning_exception.exception.*;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.PrintWriter;

public class HomeLibrary implements AutoCloseable{

    private Book [] book;
    public int numberOfBook; //make it private
    private PrintWriter file;

    private void setNumberOfBook() throws java.util.InputMismatchException  {

        Scanner sc = new Scanner(System.in);
        int num = 0;
        while(num<=0) {
            try {
                System.out.print("Enter the number of book: ");
                num = sc.nextInt();
                if (num <= 0) {
                    throw new  BelowOrEqualZero();
                }
            }catch (BelowOrEqualZero ex) {
                ex.what();
                System.out.println("Size can't be below or equal zero. Try again");
            }
        }
        numberOfBook = num;
    }


    public void addToFile(){

        System.out.println();
        System.out.println("Adding to file");
        try {
            file = new PrintWriter("homeLibrary.txt");
            for(int index = 0; index<numberOfBook; index++) {
                file.println();
                file.print(index+1 + "\t");
                file.println(book[index].toString());
            }

        } catch(FileNotFoundException ex) {
            System.out.println("File is not found");
        }

        for(int index = 0; index< 3; index++) {
            System.out.print("*\t\t");
            try {
                Thread.sleep(1000);
            }catch(InterruptedException ex){
                System.out.println("Interrupted exception");
            }
        }
        //finally{
          //  file.close();
        //}
    }
    public HomeLibrary(){
        boolean wrongData;
        do {
            try {
                wrongData = false;
                setNumberOfBook();
            } catch (java.util.InputMismatchException ex) {
                wrongData = true;
                System.out.println("Error!Added incorrect data!");
            }
        } while(wrongData);
        book = new Book[numberOfBook];

        setBook();

    }

    private Book addBook() throws BelowOrEqualZero, java.util.InputMismatchException {
        int chosenNumber = 0;
        System.out.println("1\tComicBook\n2\tLoveStory\n3\tHorrorBook");
        do {
            System.out.println("Choose appropriate genre(1-3)");
            Scanner sc = new Scanner(System.in);
            chosenNumber = sc.nextInt();
            if (chosenNumber <= 0) {
                throw new BelowOrEqualZero();
            }
        } while (chosenNumber != 1 && chosenNumber != 2 && chosenNumber != 3);


        switch (chosenNumber) {
            case 1:
                return new ComicBook();
            case 2:
                return new LoveStory();
            case 3:
                return new HorrorBook();
        }
        return null;
    }
    public String getBook(int index){

        return book[index].toString();
    }


    public void setBook() {
        for (int index = 0; index < numberOfBook;) {

            Book tempBook;
            boolean wrongData;
            do {
                try {
                    wrongData = false;
                    tempBook = addBook();
                    if(tempBook!=null) {
                        book[index] = tempBook;
                        System.out.println(index + 1 + " added!");
                        index++;
                    }
                    else{
                        System.out.println("Book was not added");
                    }
                } catch (BelowOrEqualZero ex) {
                    wrongData = true;
                    ex.what();
                } catch (java.util.InputMismatchException ex) {
                    wrongData = true;
                    System.out.println("Incorrent input!");
                }
            }
            while (wrongData);
        }
    }
    @Override
    public void close() throws IOException {
        if(numberOfBook>10)
            throw new IOException("Too many books");
        System.out.println("\nClosing");
        if(file!=null) {
            file.close();
        }
    }
}