package com.learning_exception.basicClasses;

import com.learning_exception.exception.*;
import java.util.Scanner;

public class HorrorBook extends Book {

    int year;
  public  HorrorBook() throws TooBigValue, BelowOrEqualZero{

      super.setNameOfBook();
      super.setNameOfAuthor();
        genre = "horror";
        checkYear();
    }

    private void checkYear() throws TooBigValue, BelowOrEqualZero {


        Scanner sc = new Scanner(System.in);
        int addedYear = 0;
        boolean wrongData;
        do{
            wrongData = false;
            System.out.print("Enter the year");
            addedYear = sc.nextInt();
            try {
                if (addedYear >= 2020) {
                    throw new TooBigValue("year");
                } else if (addedYear <= 0) {
                    throw new BelowOrEqualZero();
                }
            } catch (TooBigValue ex) {
                wrongData = true;
                ex.what();
                System.out.println("Try again!");
            } catch (BelowOrEqualZero ex) {
                wrongData = true;
                ex.what();
                System.out.println("Try again!");
            }

        }while(wrongData);
        year = addedYear;
    }


    @Override
    public String getGenre() {
        return genre;
    }

    public int getYear(){
        return year;
    }

    @Override
    public String toString() {
        return super.toString() + "\t" + year + "year";
    }
}