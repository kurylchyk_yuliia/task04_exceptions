package com.learning_exception.basicClasses;


public class ComicBook extends Book {

    ComicBook(){
        super.setNameOfBook();
        super.setNameOfAuthor();
        genre = "comic book";
    }



    @Override
    public String getGenre() {
        return "story about superheroes";
    }


}