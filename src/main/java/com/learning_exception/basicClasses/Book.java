package com.learning_exception.basicClasses;
import java.util.Scanner;

public  abstract class  Book {
    protected String nameOfBook = "";
    protected String nameOfAuthor="";
    protected String genre ="";

    public String getName(){
        return  nameOfBook;
    };

    public void setNameOfBook() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the name of book:");
            nameOfBook = sc.nextLine();
        } catch(NullPointerException ex){
            System.out.println("You entered null");
        }
    }

    public void setNameOfAuthor(){
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the name of author");
            nameOfAuthor = sc.nextLine();
        }catch(NullPointerException ex){
            System.out.println("You entered null");
        }


    }

    public abstract String getGenre();

    @Override
    public String toString() {
        return nameOfBook + "\t" + nameOfAuthor +"\t"+getGenre();
    }
}