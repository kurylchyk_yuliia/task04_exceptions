package com.learning_exception;
import com.learning_exception.basicClasses.HomeLibrary;
import java.io.IOException;

public class Controller {

    private View v;

    Controller(){
        v = new View();
        CreateHomeLibrary();
    }

    public void CreateHomeLibrary() {

        try (HomeLibrary hl = new HomeLibrary()) {
            v.showMessage("\nAll your books: ");
            for (int index = 0; index < hl.numberOfBook; index++) {
                v.showMessage(hl.getBook(index));
            }
            hl.addToFile();
        } catch (IOException ex) {
            v.showError("Too many books to add");
            //just to see how exception works in close() method
        }
    }
}